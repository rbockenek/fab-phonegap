$(function(){

    var data = [{
            label: 'Oct \'13',
            rVal: 5,
            yVal: 0,
            xVal: 1,
            'class': 'spotify-year-bubble'
        }, {
            label: 'Nov \'13',
            rVal: 4,
            yVal: -0,
            xVal: 2.15,
            'class': 'spotify-year-bubble'
        }, {
            label: 'Dec \'13',
            rVal: 5,
            yVal: 0,
            xVal: 3.20,
            'class': 'spotify-year-bubble'
        }, {
            label: 'Jan \'14',
            rVal: 7,
            yVal: 1,
            xVal: 4.30,
            'class': 'spotify-year-bubble-selected',
            'id': "selected-item"

        }, {
            label: 'Feb \'14',
            rVal: 5,
            yVal: 0,
            xVal: 5.45,
            'class': 'spotify-year-bubble'
        }, {
            label: 'Mar \'14',
            rVal: 6,
            yVal: 0,
            xVal: 6.55,
            'class': 'spotify-year-bubble'
        }, {
            label: 'Apr \'14',
            rVal: 8,
            yVal: 1,
            xVal: 7.65,
            'class': 'spotify-year-bubble'
        }, {
            label: 'May \'14',
            rVal: 7,
            yVal: 1,
            xVal: 8.75,
            'class': 'spotify-year-bubble'
        }, {
            label: 'Jun \'14',
            rVal: 6,
            yVal: 1,
            xVal: 9.85,
            'class': 'spotify-year-bubble'
        }, {
            label: 'Jul \'14',
            rVal: 5,
            yVal: 1,
            xVal: 10.95,
            'class': 'spotify-year-bubble'
        }],

    // Preliminaries
    // domain is the data domain to show
    // range is the range the values are mapped to
        svgElm = d3.select('svg'),
        rscale = d3.scale.linear().domain([0, 5])
            .range([0, 60]),
        xscale = d3.scale.linear().domain([0, 5])
            .range([0, 320]),
        yscale = d3.scale.linear().domain([0, 5])
            .range([150, 0]),
        circles;

// Circles now easily reusable
    circles = svgElm.select('g.year-data-group')
        .selectAll('circle')
        .data(data)
        .enter()
        .append('circle');

    

// Alter circles
    circles.attr('class', function (d) {
        return d['class'];
    })
        .attr("id", function (d){
            return d['id']
        })
        .attr('r', 0)
        .attr('cx', function (d) {
            return xscale(d.xVal);
        })
        .attr('cy', function (d) {
            return yscale(d.yVal);
        })
    ;

    circles.transition()
            .delay(800)
            .duration(3000)
            .ease("elastic")
            .attr("r", function (d) { return rscale(d.rVal); });

    svgElm.append('text').text('32,000')
        .attr('x', 230)
        .attr('y', 120)
        .attr('fill', 'white')
        .attr("font-size", 30)
        .attr("opacity", 0)
        .transition()
        .delay(1400)
        .duration(1000)
        .attr("opacity", 1 );
    svgElm.append('text').text('January 2014')
        .attr('x', 230)
        .attr('y', 140)
        .attr('fill', 'white')
        .attr("font-size", 16)
        .attr("opacity", 0)
        .transition()
        .delay(1400)
        .duration(1000)
        .attr("opacity", 1 );

//    circles.style("opacity", 0)
//
//    circles.transition()
//        .style("opacity", 0.5)
//        .duration(1000) // this is 1s


    var map_bubble = new Datamap({
        element: document.getElementById("arcs"),
        scope: 'usa',
        fills: {
            defaultFill: "none",
            'tomorrow': "#1da7a8",
            'thorn': "#828534",
            'rain': "#b7b633",
            'dreams': "#9ac93d"
        },
        geographyConfig: {
            popupOnHover: false, //disable the popup while hovering
            highlightOnHover: false
        },
        bubbleConfig: {
            highlightOnHover: false
        }
    });

    var bubbless = [{
        name: "Bubble1",
        radius: 45,
        borderColor: "none",
        fillOpacity: 0.9,
        fillKey: 'thorn',
        latitude: 30.300474,
        longitude: -97.747247,
        highlightOnHover: false
    },{
        name: "Bubble2",
        radius: 30,
        borderColor: "none",
        fillOpacity: 0.9,
        fillKey: 'tomorrow',
        latitude: 28.543005,
        longitude: -82.394738,
        highlightOnHover: false
    },{
        name: "Bubble3",
        radius: 15,
        borderColor: "none",
        fillOpacity: 0.9,
        fillKey: 'dreams',
        latitude: 45.891952,
        longitude: -122.813205,
        highlightOnHover: false
    },{
        name: "Bubble4",
        radius: 50,
        borderColor: "none",
        fillOpacity: 0.9,
        fillKey: 'dreams',
        latitude: 33.531525,
        longitude: -117.769043,
        highlightOnHover: false
    },{
        name: "Bubble5",
        radius: 40,
        borderColor: "none",
        fillOpacity: 0.9,
        fillKey: 'tomorrow',
        latitude: 40.71448,
        longitude: -74.00598,
        highlightOnHover: false
    },{
        name: "Bubble6",
        radius: 7,
        borderColor: "none",
        fillOpacity: 0.9,
        fillKey: 'rain',
        latitude: 40.71448,
        longitude: -80.00598,
        highlightOnHover: false
    },{
        name: "Bubble7",
        radius: 15,
        borderColor: "none",
        fillOpacity: 0.9,
        fillKey: 'dreams',
        latitude: 35.582494,
        longitude: -78.408851,
        highlightOnHover: false
    }
    ];

    map_bubble.bubbles(bubbless);
    var circle_in_map = d3.selectAll('.datamap circle');
    circle_in_map
            .transition()
            .attr("r", function (d) { return 0; })
            .transition()
            .delay(800)
            .duration(3000)
            .ease("elastic")
            .attr("r", function (d) { return d.radius; });


    var track_data = [{
        name: "When Tomorrow Comes",
        value: 2000
    },{
        name: "Thorn In My Side",
        value: 3000
    },{
        name: "Here Comes The Rain Again",
        value: 4000
    },{
        name: "Sweet Dreams (Are Made of This)",
        value: 11000
    }];
    var total = 14000;

    var color = ["#1da7a8", "#828534","#b7b633", "#9ac93d"];

    var pie = d3.layout.pie()
        .sort(null);

    var track_value = [{
        x: -70,
        y: 40
    },{
        x: -100,
        y: 10
    },{
        x: -120,
        y: -35
    },{
        x: 110,
        y: 50
    }];

    function draw (i) {
        var arc = d3.svg.arc()
            .innerRadius(70 + i*20)
            .outerRadius(80 + i*20)
            .startAngle(function(d) { return d.startAngle + Math.PI; })
            .endAngle(function(d) { return d.endAngle + Math.PI; });

        var svgTrack = d3.select("#trackChart").append("svg")
            .attr("width", 320)
            .attr("height", 300)
            .attr("class", "trackChart-svg");

        var g = svgTrack.append("g")
            .attr("transform", "translate(" + 150 + "," + 150 + ")");

        g.selectAll('path')
            .data(pie([track_data[i].value, total - track_data[i].value]))
            .enter().append("path")
            .attr("fill", function(d, idx){ return color[i];})
            //.attr("d", arc) //comment this line if animation is added
            .transition()
            .delay(800)
            .ease("quad")
            .duration(1700)
            .attrTween("d", function (b) {
                var i = d3.interpolate({startAngle: 0, endAngle: 0}, b);
                return function(t) { return arc(i(t)); };
            });

        g.append('text').text(function(d){ return track_data[i].name;})
            .attr('text-anchor', 'left')
            .attr('fill', '#CCC')
            .attr("font-size", 11)
            .attr("x", 200)
            .attr("y", 80 + i*20)
            .transition()
            .delay(500)
            .ease("bounce")
            .duration(1000)
            .attr("x", 5);
        
        g.append('text').text(function(){ return track_data[i].value/1000 + "K";})
            .attr('text-anchor', 'left')
            .attr('fill', '#CCC')
            .attr("font-size", 18)
            .attr("x", track_value[i].x)
            .attr("y", track_value[i].y)
            .attr("opacity", 0)
            .transition()
            .delay(2400)
            .duration(1000)
            .attr("opacity", 1 );
    }       

    for (var i = 0; i < track_data.length; i++) {

        draw(i);

    };

    //bring selected to front 
    d3.select(".spotify-year-bubble-selected").moveToFront();



    $("#trackChart g > path:last-of-type").hide();

});