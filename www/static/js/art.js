$("#hollie").click(function(){
    window.location = "art_landing.html";
});

function addProjectCarousel (ElementId) {
	var HammerProject = new Hammer($("#"+ElementId)[0])

	HammerProject.on("panleft",function(){
	  $("#"+ElementId).carousel('next');
	});

	HammerProject.on("panright",function(){
	  $("#"+ElementId).carousel('prev');
	});
}

if($("#projectCarousel").length){
	addProjectCarousel("projectCarousel");
}

if($("#projectCarouselOne").length){
	addProjectCarousel("projectCarouselOne");
}

if($("#featureCarousel").length){
	addProjectCarousel("featureCarousel");
}

if($("#contactCarouselOne").length){
	addProjectCarousel("contactCarouselOne");
}

if($("#moveCarouselOne").length){
	addProjectCarousel("moveCarouselOne");
}
