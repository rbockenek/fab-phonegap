 (function($) {
/* jQuery Tiny Pub/Sub - v0.7 - 10/27/2011
 * http://benalman.com/
 * Copyright (c) 2011 "Cowboy" Ben Alman; Licensed MIT, GPL */ 
  var o = $({});
 
  $.subscribe = function() {
    o.on.apply(o, arguments);
  };
 
  $.unsubscribe = function() {
    o.off.apply(o, arguments);
  };
 
  $.publish = function() {
    o.trigger.apply(o, arguments);
  };
 
}(jQuery));


function Init () {
	//keep page stay in full screen
	var a=document.querySelectorAll("a, area");
	for(var i=0;i<a.length;i++) {
	    if(!a[i].onclick && a[i].getAttribute("target") != "_blank") {
	        a[i].onclick=function() {
	                window.location=this.getAttribute("href");
	                return false; 
	        }
	    }
	}

	//ipad safari: disable scrolling, and bounce effect

};
Init();





// To make images retina, add a class "2x" to the img element
// and add a <image-name>@2x.png image. Assumes jquery is loaded.
 
function isRetina() {
	var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),\
					  (min--moz-device-pixel-ratio: 1.5),\
					  (-o-min-device-pixel-ratio: 3/2),\
					  (min-resolution: 1.5dppx)";
 
	if (window.devicePixelRatio > 1)
		return true;
 
	if (window.matchMedia && window.matchMedia(mediaQuery).matches)
		return true;
 
	return false;
};
 
 
function retina() {
	
	if (!isRetina())
		return;
	
	$("img.2x").map(function(i, image) {
		
		var path = $(image).attr("src");
		
		path = path.replace(".png", "@2x.png");
		path = path.replace(".jpg", "@2x.jpg");
		
		$(image).attr("src", path);
	});
};

$(document).ready(retina);


$(function() {
	var edit_active = false;
	// var tl = new TimelineMax();
	// tl.append(TweenMax.to(".fab-loading-div", 0.5, {opacity: 0, ease: Ease.easeOut, onComplete: function(){ $('.fab-loading-div').remove(); }}));
	// tl.append(TweenMax.staggerFrom(".panel", 0.3, {scale:0.1, autoAlpha:0, ease: Ease.easeOut}, 0.03));

	  var tl = new TimelineMax({
	             autoRemoveChildren: true,
	             onComplete : function(){
	                 $.publish("animateFinish");
	             },
	             align: 'sequence',
	             tweens : [
	                 TweenMax.to(".fab-loading-div", 0.5, {opacity: 0, ease: Ease.easeOut, onComplete: function(){ $('.fab-loading-div').remove(); }}),
	                 TweenMax.staggerFrom(".panel", 0.3, {scale:0.1, autoAlpha:0, ease: Ease.easeOut}, 0.03)
	             ]
	         });

/*	$(".panel, .well").addClass('fadeInDown animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
   		$(this).removeClass('fadeInDown animated');
    });*/

    // $("#footer").addClass('fadeInUp animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
   	//  	$(this).removeClass('fadeInUp animated');
    // });



	/*######################## Click setting icon #########################*/
	$(".panel-content .fa-info-circle").click(function(e) {
		e.stopPropagation();
		e.preventDefault();

		var this_card = $(this).closest(".card");

		var this_content = this_card.find(".panel-content");

		var this_setting = this_card.find(".panel-setting");

		this_content.hide();

		this_setting.show().addClass('flipInY animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	   		$(this).removeClass('flipInY animated');
	    });
	});

	$(".panel-setting .fa-info-circle").click(function(e) {
		e.stopPropagation();
		e.preventDefault();

		var this_card = $(this).closest(".card");

		var this_content = this_card.find(".panel-content");

		var this_setting = this_card.find(".panel-setting");

		this_setting.hide();

		this_content.show().addClass('flipInY animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	   		$(this).removeClass('flipInY animated');
	    });
	    return false;
	});
	

	/*######################## Click setting Account card #########################*/
	$(".card .panel-content").click(function(event) {

		var account_detail_wraper = $(".account_detail_wraper");
		if (!edit_active){
			if (account_detail_wraper.is(':visible')){
//			    hideDetails();

			} else {
//				showDetails();
			}
		}
	});

	/*######################## SHOW/HIDE FOOTER #########################*/
	$(".footer_switch").click(function(event) {
		var footer = $("#footer");
		if(footer.is(":visible")){
			TweenMax.to(footer, 0.2, {css:{autoAlpha:0, bottom: '-90px'}, ease: Back.easeIn, onComplete: function() {
		    	footer.hide();
		    	$("#footer_expand").fadeIn("fast");
		    }});
	    } else {
	    	footer.show();
	    	$("#footer_expand").fadeOut("fast");
	    	TweenMax.to(footer, 0.2, {css:{autoAlpha:1, bottom: '0px'}, ease: Back.easeOut, onComplete: function() {
	    		//compelete
		    }});
	    }

	});


	/*######################## EDIT VIEW #########################*/
	
	$(".edit_view").click(function(event) {
		account_panel = $(".panel");
		if (!edit_active){
			$(this).addClass('active');

			$(account_panel).parent().sortable();
			$(account_panel).parent().disableSelection();
			

			var account_detail_wraper = $(".account_detail_wraper");
			TweenMax.to(account_panel, 0.5, {css:{'box-shadow': '0 4px 12px rgba(0, 0, 0, 0.35)', top: '5px', scale: 0.95}, ease: Back.easeOut.config(3)});
			if (account_detail_wraper.is(':visible')){
			    hideDetails();
			}
			edit_active = true;

			$(".card4, .card5").slideDown();
		} else {
			TweenMax.to(account_panel, 0.5, {css:{'box-shadow': '0 1px 2px rgba(0, 0, 0, 0.15)', top: '0px', scale: 1}, ease: Back.easeIn.config(6)});
			edit_active = false;
			$(this).removeClass('active');
			if($('.view_all_accounts').hasClass('active') == false){
				$(".card4, .card5").slideUp();
			}
		}
	});
	
	//TweenMax.to(account_panel, 2, {css:{scale: 1.1}, ease: Ease.easeOut, repeat:3, yoyo:true});
	//Draggable.create(".panel", {type:"x,y", edgeResistance:0.05, bounds:".container-fluid", throwProps:false});

	/*######################## Add Search #########################*/

	$(".container-fluid").after('<div class="black-overlay black-search-overlay"></div>'
								+'<div class="search-content overlay-content"></div>'
								+'<div class="ask-content overlay-content"></div>'
								+'<div class="share-content overlay-content"></div>'
								+'<div class="move-content overlay-content"></div>'
								+'<div class="curate-content overlay-content"></div>'
								);
	var footer_menus = ['ask', 'share', 'search', 'move', 'curate'];
	for (var i in footer_menus ) {
		$("."+footer_menus[i]+"-content").load(""+footer_menus[i]+"_tmp.html", function () {
			$(".close-search-overlay a, #confirm-one, #cancel-overlay, #curate-one, #cancel-overlay").click(function(e) {
				e.stopPropagation(); e.preventDefault();
				$(this).parents('.overlay-content').hide();
				$(".black-search-overlay").hide();
				$(".content-wraper.container-fluid").removeClass('fixed-height');
			});

			$(".tags-input").keyup(function(e) {
				if(e.keyCode === 188) {
					var caretPos = $(this)[0].selectionStart;
					var textAreaTxt = $(this).val();
					var txtToAdd = " #";
					$(this).val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
				}
			});
		});

		$(".nav-"+footer_menus[i]).click(function(e) {
			e.stopPropagation(); e.preventDefault();
			var overlay = $(this).data('overlay');
			$("."+overlay+"-content, .black-search-overlay").show();

			$(".content-wraper.container-fluid").addClass('fixed-height');


			if($("."+overlay+"-content input").length){
				$("."+overlay+"-content input:first").focus();
			} else if($("."+overlay+"-content textarea").length){
				$("."+overlay+"-content textarea").focus();
			}

		});
	};


	/*######################## CLICK MSG #########################*/
	$(".msg_menu").click(function(event) {

		if($(this).hasClass('active') == false) {
			$(this).addClass('active');
			$(".message_container").show();
			TweenMax.to($(".message_container"), 0.3, {css:{left: '0px'}, ease: Back.easeInOut});
		} else {
			$(this).removeClass('active');
			TweenMax.to($(".message_container"), 0.3, {css:{left: '-360px'}, ease: Back.easeInOut, onComplete: function () {
				$(".message_container").hide();
			}});
			
		}
	});


	//bring d3 object to front with d3

	d3.selection.prototype.moveToFront = function() {
	  return this.each(function(){
	    this.parentNode.appendChild(this);
	  });
	};

});


