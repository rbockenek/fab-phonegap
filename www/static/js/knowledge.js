$(function(){

    var track_data = [{
        name: "Advanced Photography",
        value: 15
    },{
        name: "FABIQ Entrepreneurship",
        value: 45
    },{
        name: "iPhone Social Networking",
        value: 53
    },{
        name: "Film Finance 101",
        value: 95
    }];
    var total = 133;

    var color = ["#1da7a8", "#828534","#b7b633", "#9ac93d"];

    var pie = d3.layout.pie()
        .sort(null);

    var track_value = [{
        x: -60,
        y: 33
    },{
        x: -65,
        y: -47
    },{
        x: -50,
        y: -80
    },{
        x: 100,
        y: 0
    }];

    function draw (i) {
        var arc = d3.svg.arc()
            .innerRadius(50 + i*20)
            .outerRadius(60 + i*20)
            .startAngle(function(d) { return d.startAngle + Math.PI; })
            .endAngle(function(d) { return d.endAngle + Math.PI; });

        var svgTrack = d3.select("#trackChart").append("svg")
            .attr("width", 300)
            .attr("height", 300)
            .attr("class", "trackChart-svg");

        var g = svgTrack.append("g")
            .attr("transform", "translate(" + 150 + "," + 150 + ")");

        g.selectAll('path')
            .data(pie([track_data[i].value, total - track_data[i].value]))
            .enter().append("path")
            .attr("fill", function(d, idx){ return color[i];})
            //.attr("d", arc) //comment this line if animation is added
            .transition()
            .delay(800)
            .ease("quad")
            .duration(1700)
            .attrTween("d", function (b) {
                var i = d3.interpolate({startAngle: 0, endAngle: 0}, b);
                return function(t) { return arc(i(t)); };
            });

        g.append('text').text(function(d){ return track_data[i].name;})
            .attr('text-anchor', 'left')
            .attr('fill', '#CCC')
            .attr("font-size", 11)
            .attr("x", 200)
            .attr("y", 60 + i*20)
            .transition()
            .delay(500)
            .ease("bounce")
            .duration(1000)
            .attr("x", 5);
        
        g.append('text').text(function(){ return track_data[i].value + "%";})
            .attr('text-anchor', 'left')
            .attr('fill', '#CCC')
            .attr("font-size", 18)
            .attr("x", track_value[i].x)
            .attr("y", track_value[i].y)
            .attr("opacity", 0)
            .transition()
            .delay(2400)
            .duration(1000)
            .attr("opacity", 1 );
    }

    



    for (var i = 0; i < track_data.length; i++) {

        draw(i);

    };

    $("#trackChart g > path:last-of-type").hide();

});