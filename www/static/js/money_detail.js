function selectAccount(select_element, elID){

    $(".account").removeClass('active');
    $('#'+elID).addClass('active');
    $("#accountBubble").children().remove();

    var r = 280;
    var width = 784,
        height = 280,
        format = d3.format(", d"),
        color = d3.scale.category20c()
            .range(["#b61e75", "#d21f34", "#e7533d", "#f5b819", "#f8d10b", "#99ca3c", "#007d7e", "#1da8a9"]);

    var layout_gravity = 1,
        damper = 0.1;


    var bubble = d3.layout.pack()
        .sort(null)
        .size([width, height])
        .padding(8.5);

    var svg = d3.select("#accountBubble").append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("class", "bubble");

    //var center = {x: width / 2, y: height / 2};

    var category_centers = {};

    d3.csv("static/data/Transaction-Details.csv", function(error, data) {

        var all_categories_map = {},
            all_categories =[],
            complete_data_map = {},
            complete_data = {};

        for (var i = 0; i < data.length; i++) {

            if(!complete_data_map[data[i]["Account Name"]]){

                for (var b = 0; b < data.length; b++) {
                    data[b]['value']= Math.abs(parseFloat( data[b]["Amount"] ));

                    if(data[b]["Account Name"] == data[i]["Account Name"]){

                        if(!all_categories_map[data[b]["Category"]]) {
                            all_categories_map[data[b]["Category"]] = {
                                "totalAmount": 0,
                                "Category": data[b]["Category"]
                            }
                        }

                        all_categories_map[data[b]["Category"]]["totalAmount"] += data[b]['value'];

                        //limit the max bubble size and min bubble size
                        if (data[i]['value'] >= 20000) {
                            data[i]['value'] = 20000;
                        }
                        if (data[i]['value'] <= 400) {
                            data[i]['value'] = 400;
                        }

                    }

                }

                complete_data[data[i]["Account Name"]] = all_categories_map;
                all_categories_map = {};
            }

        }

        for (var key in complete_data[select_element]) {
            all_categories.push(complete_data[select_element][key]);
        }

        //sort all categories by total amount
        all_categories.sort(function(a, b) {
            return  b.totalAmount - a.totalAmount;
        });

        //caculate category center position x and y
        for (var i = 0; i < all_categories.length; i++) {
            if(!category_centers[all_categories[i]["Category"]]){
                category_centers[all_categories[i]["Category"]] = {x: (i * width / 5), y: height / 2};
            }
        }

        console.log(category_centers);

        // Returns a flattened hierarchy containing all leaf nodes under the root.
        data = {children: data};
        console.log(data);
        var node = svg.selectAll("g.node")
            .data(bubble.nodes(data).filter(function(d) { return !d.children; }))
            .enter().append("g")
            .attr("class", "node")

        node.append("title")
            .text(function(d) { return "["+d.Category +"] : " + format(d.Amount); });

        var circles = node.append("circle")
            .attr("r", 10)
            .style("fill", function(d) { return category_centers[d.Category]? color(d.Category) : "#1da8a9"; })
            .attr("class", function(d){return d.Category})
//        .attr("transform", function(d) { return "translate(" + d.x/30 + "," + d.y/15 + ")"; });

        circles.transition().duration(1500).attr("r", function(d) { return d.r; });

        function classes(root) {
            var classes = [];
            function recurse(name, node) {
                if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
                else classes.push({packageName: name, className: node.name, value: node.size});
            }
            return {children: classes};
        }

        function charge(d) {
            return -Math.pow(d.r, 2.0) / 8;
        }

        function start() {
            //console.log(node);
            force = d3.layout.force()
                //.nodes([])
                .size([width, height]);
        }



        function display_by_year() {
            force.gravity(layout_gravity)
                .charge(charge)
                .friction(0.9)
                .on("tick", function(e) {
                    //console.log(circles)
                    circles.each(move_towards_year(e.alpha))
                        .attr("cx", function(d) {return d.x+80;})
                        .attr("cy", function(d) {return d.y;});
                });
            force.start();
            display_years();
        }

        function move_towards_year(alpha) {
            return function(d) {
                var target = category_centers[d.Category];
                if(target){
                    d.x = d.x + (target.x - d.x) * (damper + 0.04) * alpha * 2.7;
                    d.y = d.y + (target.y - d.y) * (damper + 0.004) * alpha * 0.8;
                } else {
                    d.x = d.x + (-50 - d.x) * (damper + 0.02) * alpha * 1.1;
                    d.y = d.y + (height / 2 - d.y) * (damper + 0.02) * alpha * 1.1;
                }
            };
        }


        function display_years() {

            //var years_x = {"Uncategorized": width / 5, "Restaurants": 2 * width / 5, "Groceries": 3 * width / 5, "Shopping": 4 * width / 5, "Furnishings": width};
            var years_data = d3.keys(category_centers);
            var years = svg.selectAll(".years")
                .data(years_data);

            years.enter().append("text")
                .attr("class", "years")
                .attr("x", function(d) { return category_centers[d]["x"] + 80; }  )
                .attr("y", 40)
                .attr("text-anchor", "middle")
                .attr("fill",'#FFFFFF')
                .attr("font-size", "13px")
                .text(function(d) { return d;});

        }


        start();

        display_by_year();


    });

    d3.select(self.frameElement).style("height", height + "px");

}

selectAccount("FAB Business","FABBusiness");
