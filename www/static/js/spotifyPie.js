(function(){

    var spotifyChartRendering = function(){
        var dataset = {
            accounts: [
                63245,
                53245,
                28479,
                64959
            ]
        };

        var spotify_balance = "58,244"

        var colors = ["#1da7a8", "#828534", "#b7b633", "#9ac93d"];
        var width = 287,
            height = 184,
            radius = Math.min(width, height) / 2;

        var color = d3.scale.ordinal()
            .domain(dataset.accounts)
            .range(colors);

        var pie = d3.layout.pie()
            .sort(null);

        var arc = d3.svg.arc()
            .innerRadius(0)
            .outerRadius(function(d,i) { return radius - i*5 - 20; });

        var svg = d3.select("#spotify_pie").append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox','0 0 '+Math.min(width,height) +' '+Math.min(width,height) )
            .attr('preserveAspectRatio','xMinYMin')
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


        var path = svg.selectAll("path")
            .data(pie(dataset.accounts))
            .enter().append("path")
            .attr("fill", function(d, i){return color(i);})
            // .attr("d", arc); //comment this line if animation is added
            .transition()
            .ease("exp")
            .duration(600)
            .attrTween("d", function (b, i, a) {
                var j = d3.interpolate({startAngle: 0.1*Math.PI, endAngle: 0.1*Math.PI}, b);
                return function(t) { arc.outerRadius(radius - i*5 - 20); return arc(j(t)); };
            });

        svg.append("text")
            .text('$ '+spotify_balance)
            .attr("text-anchor", "middle")
            .style("font-size",'22px')
            .attr("fill",'#FFFFFF')
            .attr("dy", 6)
            .attr("dx",2);

        for (var i = 0; i <= colors.length; i++){
            $("#spotify-item-"+ (i+1)).css('border-left', "5px solid"+ colors[i]);
        }
    }

    $.subscribe("animateFinish", spotifyChartRendering);

})();