$(function() {

    /*######################## AutoComplete for Search #########################*/

    $.getJSON( "static/data/search.json", function( data ) {

        var list = [];

        for (var i = 0; i < data.length; i++) {
            list.push(data[i].artist);
        }

        $( "#search" ).autocomplete({
            source: list,
            appendTo: "#search-result",
            select: function( event, ui ) {
            }
        });

    });

    /*######################## AutoComplete for Ask #########################*/

    $.getJSON( "static/data/ask.json", function( data ) {

        var list = [];

        for (var i = 0; i < data.length; i++) {
            list.push(data[i].question);
        }

        $( "#ask" ).autocomplete({
            source: list,
            appendTo: "#ask-result",
            select: function( event, ui ) {
            }
        });

    });

});


