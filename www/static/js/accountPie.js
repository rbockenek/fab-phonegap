(function(){
    var acountChartRendering = function(){
        var dataset = {
            accounts: [
                33245,
                28794,
                17274,
                15257,
                12847,
                8328,
                7837,
                5758
            ]
        };

        var balance = "23,000";

        var width = 287,
            height = 264,
            radius = Math.min(width, height) / 2;

        var color = d3.scale.ordinal()
            .range(["#b61e75", "#d21f34", "#e7533d", "#f5b819", "#f8d10b", "#99ca3c", "#007d7e", "#1da8a9"]);

        var pie = d3.layout.pie()
            .sort(null)
            .value(function(d) { return d.Amount; });

        var arc = d3.svg.arc()
            .innerRadius(radius - 70)
            .outerRadius(radius - 40);

        

        function arcTween(a) {
              var i = d3.interpolate(this._current, a);
              this._current = i(0);
              return function(t) {
                return arc(i(t));
              };
        }


        //load data from csv
        d3.csv("static/data/Donut-Data.csv", function(error, data) {

            dataSet = {};
            dropdownOpt = '';

            for (var i = 0; i < data.length; i++) {
                var accGroup = data[i]["Account Group"];

                if(!dataSet[accGroup] && accGroup !==""){

                    if(data[i]["Category"] !== "Topline") {
                        dataSet[accGroup] = [];
                    } else {
                        //it is the TopLine
                         dataSet[accGroup] = [];
                         dataSet[accGroup]["Topline"] = data[i]["Amount"];
                    }
                    
                    if (dropdownOpt === "") {
                        dropdownOpt += '<li class="active"><a href="#">' + accGroup + '</a></li>';
                    } else {
                        dropdownOpt += '<li><a href="#">' + accGroup + '</a></li>';
                    }

                    
                    
                }

                if (dataSet[accGroup] && data[i]["Category"] !== "Topline"){
                    dataSet[accGroup].push(data[i]);
                }
                

            }

            console.log(dataSet);

             drawPieChart(dataSet["All Personal"]);
            //add dropdown
            $(".account-dropdown .dropdown-menu").append(dropdownOpt).click(function(e){
                e.stopPropagation(); e.preventDefault();
                var accGroup = $(e.target).html();

                //change the text in the dropdown
                $(e.target).parents(".account-dropdown").removeClass("open").find("button:first-child").html(accGroup);
                $(e.target).parents(".dropdown-menu").find("li").removeClass('active');
                $(e.target).closest("li").addClass('active');

                //empty the chart
                $("#accounts_pie").empty();
                //redraw chart
                drawPieChart(dataSet[accGroup]);

            })
            

            function drawPieChart (data){
                var svg = d3.select("#accounts_pie").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .attr('viewBox','0 0 '+Math.min(width,height) +' '+Math.min(width,height) )
                    .attr('preserveAspectRatio','xMinYMin')
                    .append("g")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                var g = svg.selectAll(".arc")
                    .data(pie(data))
                    .enter()
                    .append("g")
                    // 
                    .attr("class", "arc");
            

                g.append("path")
                    //.attr("d", arc) //comment this line if animation is added
                    .attr("fill", function(d, i){return color(d.data.Category);})
                    .transition()
                    .ease("exp")
                    .duration(600)
                    .attrTween("d", function (b) {
                        var i = d3.interpolate({startAngle: 0, endAngle: 0}, b);
                        return function(t) { return arc(i(t)); };
                    });

                svg.append("text")
                    .text('$ '+data["Topline"])
                    .attr("text-anchor", "middle")
                    .style("font-size",'22px')
                    .attr("fill",'#FFFFFF')
                    .attr("dy", 6)
                    .attr("dx",2);

                //draw innter ring
                var arc_inner = d3.svg.arc()
                    .innerRadius(radius - 70)
                    .outerRadius(radius - 60);

                var svg_inner = d3.select("#accounts_pie").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .attr('viewBox','0 0 '+Math.min(width,height) +' '+Math.min(width,height) )
                    .attr('preserveAspectRatio','xMinYMin')
                    .attr("position", 'absolute')
                    .attr("class", 'accounts_pie_inner')
                    .attr("top", 58)
                    .attr("left", 0)
                    .attr("opacity", 0.25)
                    .append("g")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                var path2 = svg_inner.selectAll("path")
                    .data(pie([{"Amount":7890}]))
                    .enter().append("path")
                    .attr("fill", "#000000")
                    .attr("d", arc_inner); 

            }


        });


    }


    $.subscribe("animateFinish", acountChartRendering);

})();
