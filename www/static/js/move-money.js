function showAccounts(type){

    // Selectors
    var $from = $("#money-accounts");
    var $contacts = $("#contacts");

    // Values
    var from_person = $("#from-name").attr('title');
    var to_person = $("#to-name").attr('title');

    // Check if the Profile owner is from/to and show/hide account list according to that
//    if((from_person === "from" && type === "from") || (to_person === "from" && type === "to")) {

        if ($from.is(":hidden")) {

            $contacts.addClass("hidden");
            $from.removeClass("hidden");

        } else {

            $contacts.removeClass("hidden");
            $from.addClass("hidden");

        }
//    } else {
//
//    }
}

function replaceTo(id){

    // Selectors
    var $div = $("#" + id);
    var $form_name = $("#from-name")

    // Values
    var img_url = $div.find("img").attr("src");
    var person_name = $div.find("div.contact-name").html();
    var from_person = $form_name.attr('title');

    // Replace with selected contact
    if(from_person === "from"){

        $("#to-image").attr("src", img_url);
        $("#to-name").empty().append(person_name);

    } else {

        $("#from-image").attr("src", img_url);
        $form_name.empty().append(person_name);

    }
//
//    if($div.hasClass('nav-move')){
//        $( ".nav-move" ).trigger( "click" );
//    }
}

function replaceFrom(id){

    // Selectors
    var $div = $("#" + id);
    var $form_name = $("#from-name");
    var $form_amount = $("#from-amount");

    // Values
    var img_url = $div.find("img").attr("src");
    var person_name = $div.find("div.account-name").html();
    var from_person = $form_name.attr('title');
    var new_from_amount = $div.find("div.account-type").html();

    // Replace with selected contact
    if(from_person === "from"){

        $("#from-image").attr("src", img_url);
        $form_name.empty().append(person_name);
        $form_amount.empty().append(new_from_amount);

    } else {

        $("#to-image").attr("src", img_url);
        $("#to-name").empty().append(person_name);

    }
}

function changePayment(){

    // Selectors
    var $to_url = $("#to-image");
    var $to_name = $("#to-name");
    var $from_url = $("#from-image");
    var $from_name = $("#from-name");
    var $from_amount = $("#from-amount");
    var $button = $("#confirm-plus");

    var $from_label = $("#from-name").parents(".contact-label");
    var $to_label = $("#to-name").parents(".contact-label");

    var attrFrom = $from_name.attr('title');
    var attrTo = $to_name.attr('title');

    // Values
    var to_url = $to_url.attr('src');
    var to_name = $to_name.html();
    var from_url = $from_url.attr('src');
    var from_name = $from_name.html();
    var button = $button.html();

    var from_label_content = $from_label.clone();
    var to_label_content = $to_label.clone();

    // Switch From and To
    $from_url.attr("src", to_url);
    $from_name.empty().append(to_name);
    $from_name.attr('title', attrTo);
    $to_url.attr("src", from_url);
    $to_name.empty().append(from_name);
    $to_name.attr('title', attrFrom);

    if( attrFrom ==="from"){
        $to_name.before($from_amount);
        $from_name.removeClass('label-name account-name').addClass('contact-name');
        $to_name.addClass('label-name account-name').removeClass('contact-name');
    } else {
        $from_name.before($from_amount);
        $to_name.removeClass('label-name account-name').addClass('contact-name');
        $from_name.addClass('label-name account-name').removeClass('contact-name');
    }


    // $from_label.replaceWith(to_label_content);
    // $to_label.replaceWith(from_label_content);


    //move amount
    // $to_name.before($from_amount);

    // Change Button Value
    if(button === "Confirm &amp; Do Another"){

       $button.empty().append("Advanced Invoice");

    } else {

        $button.empty().append("Confirm &amp; Do Another");

    }

    //prevent lose focus on the amount input field
    $("#money-amount").focus();
}

function validateNumber(){

    var $money_amount = $('#money-amount');
    var $money_date =  $("#money-date");

    if($money_amount.val() > 0){

        $money_amount.parent().css("border", "1px solid #3dff2d");

    } else {

        $money_amount.parent().css("border", "1px solid #ff0000");

    }

    if($money_amount.val() > 0 && $money_date.val() !== ""){

        $("#confirm-plus").removeClass("disabled");
        $("#confirm-one").removeClass("disabled");

    } else {

        $("#confirm-plus").addClass("disabled");
        $("#confirm-one").addClass("disabled");

    }

}

function validateDate(){
    var $money_amount = $('#money-amount');
    var $money_date =  $("#money-date");


    if($money_date.val() !== ""){

        $money_date.parent().css("border", "1px solid #3dff2d");

    } else {

        $money_date.parent().css("border", "1px solid #ff0000");

    }

    if($money_amount.val() > 0 && $money_date.val() !== ""){

        $("#confirm-plus").removeClass("disabled");
        $("#confirm-one").removeClass("disabled");

    } else {

        $("#confirm-plus").addClass("disabled");
        $("#confirm-one").addClass("disabled");

    }

}