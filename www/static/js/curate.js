function addGallery(id){

    var $element_id = $("#"+id);
    var $text_val = $("#"+id+" div.label-name").html();
    var $textarea_id = $("#gallery_text");

    var $confirm_plus = $("#curate-plus");
    var $confirm_one = $("#curate-one");

    //remove placeholder text 
    $("#gallery_text .placeholder").hide();


    if($element_id.hasClass("active_curate")){

        $element_id.removeClass('active_curate');

        var temp_val = $text_val + " , ";
        var temp_textarea = $textarea_id.html().replace(temp_val, "");
        $textarea_id.empty().append(temp_textarea);

        if(temp_textarea === ""){

            $confirm_one.addClass("disabled");
            $confirm_plus.addClass("disabled")

        }

    } else {

        $confirm_one.removeClass("disabled");
        $confirm_plus.removeClass("disabled")
        $element_id.addClass("active_curate");
        $textarea_id.append($text_val + " , ");

    }

}

var heightW = $(window).height() / 10;

function scrollDown(id){
    $("#" + id).animate({ scrollTop: heightW}, 600);
    heightW += 50;
    return false;
}

function curateShare () {
    $("#curate-one").click();
    var img_url = $("#curate-content-img").prop('src');
    $(".nav-share").click();
    $(".share-content-img").prop('src', img_url);
}