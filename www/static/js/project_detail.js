(function(){

    function animateImage (argument) {
        var tl = new TimelineMax({
                   autoRemoveChildren: true,
                   onComplete : function(){
                       // $.publish("animateFinish");
                   },
                   align: 'sequence',
                   tweens : [
                        TweenMax.staggerFrom(".image_ring_outer", 2, {rotation: -270, autoAlpha:0, ease: Ease.easeOut}, 0.03),
                        TweenMax.staggerFrom(".image_ring_inner", 1.5, {rotation: 180, autoAlpha:0, ease: Ease.easeOut}, 0.03),
                        TweenMax.staggerFrom(".image_headshot", 2, {scale:0.5, autoAlpha:0, ease: Elastic.easeOut}, 0.03)
                   ]
               });
    }

    function drawRoiChart () {

        var dataset = {
            ROI: [
                21,
                79
            ]
        };

        var ROI = "21%"

        var colors = ["#96c93e", "#828534"];
        var width = 180,
            height = 180,
            radius = Math.min(width, height) / 2;

        var color = d3.scale.ordinal()
            .domain(dataset.ROI)
            .range(colors);

        var pie = d3.layout.pie()
            .sort(null);

        var arc = d3.svg.arc()
            .innerRadius(radius - 35)
            .outerRadius(radius - 20);

        var svg = d3.select("#ROIChart").append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr('viewBox','0 0 '+Math.min(width,height) +' '+Math.min(width,height) )
            .attr('preserveAspectRatio','xMinYMin')
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        svg.append("circle")
            .attr("cx", 0)
            .attr("cy", 0)
            .attr("r", 60)
            .attr("fill", "#1c2028");

        var path = svg.selectAll("path")
            .data(pie(dataset.ROI))
            .enter().append("path")
            .attr("fill", function(d, i){return color(i);})
            .attr("d", arc)
            .attr("class", "ROI-arc")
            .transition()
            .ease("exp")
            .duration(1500)
            .attrTween("d", function (b) {
                var i = d3.interpolate({startAngle: 0, endAngle: 0}, b);
                return function(t) { return arc(i(t)); };
            });



        svg.append("text")
            .text(ROI)
            .attr("text-anchor", "middle")
            .style("font-size",'22px')
            .attr("fill",'#FFFFFF')
            .attr("dy", 6)
            .attr("dx",2);

        svg.append("text")
            .text("ROI")
            .attr("text-anchor", "middle")
            .style("font-size",'14px')
            .attr("fill",'#FFFFFF')
            .attr("dy", 24)
            .attr("dx",2);

        $(".ROI-arc:last").hide();

    }
    var qs = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'));

    console.log(qs.page);
    if(qs.source){
        //Highlight source in sidebar menu
        $(".nav-sidebar .nav-"+qs.source).addClass('active');
    }
    // load data template
    $.get('static/template-hogan/tpl_project_detail.html', function(html) {
        // console.log(html);    
        $.getJSON('static/data/project_detail.json', function(data) {
            console.log(data);
            var template = Hogan.compile(html);
            var output = template.render(data[qs.page - 1]);
            $(".project-content").replaceWith(output);

            drawRoiChart();

            // setTimeout(function(){
            //     animateImage()
            // }, 1000);
            animateImage();

            //keep page stay in full screen
            Init();

            //Highlight sidebar drawer
            $("#project-side-"+qs.page).addClass('active');
            $(".sidebar-list-wraper").animate({
                scrollTop: $("#project-side-"+qs.page).offset().top
            },1500, function () {
                // $("#project-side-"+qs.page).addClass('animated pulse');
            });

        });
        
    });

})();